*** Settings ***
Library    SeleniumLibrary  run_on_failure=Nothing
Library    BuiltIn
Library    String
Suite Setup        Open Browser    about:blank    chrome
Suite Teardown     Close All Browsers
*** Variable ***

${BROWSER}   Chrome
*** Test Cases ***
Test Selenium library
    Open Browser	 https://www.google.com   ${BROWSER}
    Set Screenshot Directory	 EMBED
    Wait Until Page Contains Element  name=q
    Input text   name=q   แปลภาษา
    Press Keys   name=q   ENTER
    Wait Until Element Is Enabled  ID=tw-source
    Input text   ID=tw-source-text-ta   Hi
    Capture Page Screenshot
    # [Teardown]  Close All Browsers

*** Keywords ***

